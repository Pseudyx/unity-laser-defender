﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaveDisplay : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Text waveText = GetComponent<Text>();
		waveText.text = WaveKeeper.wave.ToString();
		WaveKeeper.Reset();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
