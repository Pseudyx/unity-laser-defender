﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {
	public AudioClip[] playList;
	public bool usePlaylist = false;
	
	static MusicManager instance = null;
	int currentClipIndex;
	
	public AudioClip startClip;
	public AudioClip gameClip;
	public AudioClip endClip;
	
	
	AudioSource PlayListManager{
		get {
			return instance.GetComponent<AudioSource>().GetComponent<AudioSource>();
		}
	}
	
	void Awake(){
		if(instance != null && instance != this){
			Destroy(gameObject);
			//Debug.Log("duplicate music manager self-destroy");
		} else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			currentClipIndex = 0;
			
			if(!usePlaylist){
				PlayListManager.clip = startClip;
				PlayListManager.loop = true;
				PlayListManager.Play();
			}
		}
	}
	

	void OnLevelWasLoaded (int level) {
		PlayListManager.Stop();
		
		switch(level){
			case 0:
				PlayListManager.clip = startClip;
				break;
			case 1:
				PlayListManager.clip = gameClip;
				break;
			case 2:
				PlayListManager.clip = endClip;
				break;
		}
		
		PlayListManager.loop = true;
		PlayListManager.Play();
		
	}
	
	// Update is called once per frame
	void Update () {
		if(usePlaylist){
			bool isFinished = (!this.GetComponent<AudioSource>().isPlaying);
			
			if(isFinished){
				PlayNext();
			}
		}
	}
	
	void PlayNext(){
		PlayListManager.Stop();
		currentClipIndex++;
		if(currentClipIndex >= playList.Length){
			currentClipIndex = 0;
		}
		PlayListManager.clip = playList[currentClipIndex];
		PlayListManager.Play();
	}
}
