﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

	public static int score = 0;
	Text scoreTxt;
	
	void Start(){
		scoreTxt = GetComponent<Text>();
		Reset();
	}
	
	public void Score(int points){
		score += points;
		scoreTxt.text = score.ToString();
	}
	
	public static void Reset(){
		score = 0;
	}
}
