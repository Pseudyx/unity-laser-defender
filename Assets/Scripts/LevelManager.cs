﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public void QuitRequest () {
		//Debug.Log("Player selected quit");
		Application.Quit();
	}
	
	public void LoadLevel (string name) {
		//Debug.Log("Level load request for: " + name);
		Application.LoadLevel(name);
	}
	
	public void LoadNextLevel(){
		Application.LoadLevel(Application.loadedLevel + 1);
	}
	
}
