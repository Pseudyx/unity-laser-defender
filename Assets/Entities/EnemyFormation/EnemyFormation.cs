﻿using UnityEngine;
using System.Collections;

public class EnemyFormation : MonoBehaviour {
	public GameObject enemyPrefab;
	public float width = 10f;
	public float height = 5f;
	public float speed = 5f;
	public float spawnDelay = 0.5f;
	
	bool movingRight = false;
	float minX;
	float maxX;
	
	WaveKeeper waveKeeper;
	
	void Start () {
		waveKeeper = GameObject.Find("Wave").GetComponent<WaveKeeper>();
		float distanceToCamera = transform.position.z - Camera.main.transform.position.z;
		minX = Camera.main.ViewportToWorldPoint(new Vector3(0,0, distanceToCamera)).x;
		maxX = Camera.main.ViewportToWorldPoint(new Vector3(1,0, distanceToCamera)).x;
		waveKeeper.NextWave();
		SpawnStep();
		
	}
	
	void SpawnFleet(){
		foreach(Transform child in transform){
			GameObject enemy = Instantiate(enemyPrefab, child.transform.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = child;
		}
	}
	
	void SpawnStep(){
		Transform freePos = NextFreePos();
		if(freePos){
			GameObject enemy = Instantiate(enemyPrefab, freePos.transform.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = freePos;
		}
		if(NextFreePos()){
			Invoke("SpawnStep", spawnDelay);
		}
	}
	
	public void OnDrawGizmos(){
		Gizmos.DrawWireCube(transform.position, new Vector3(width, height));
	}
	
	void Update () {
		if(movingRight){
			//transform.position += new Vector3(speed*Time.deltaTime,0);
			transform.position += Vector3.right * speed * Time.deltaTime;
		} else {
			transform.position += Vector3.left * speed * Time.deltaTime;
		}
		
		float formationRightEdge = transform.position.x + (0.5f * width);
		float formationLeftEdge = transform.position.x - (0.5f * width); 
		if(formationLeftEdge <= minX){
			movingRight = true;
		}else if(formationRightEdge >= maxX){
			movingRight = false;
		}
		
		if(AllMembersDead()){
			Debug.Log("All Enemies Dead");
			waveKeeper.NextWave();
			SpawnStep();
		}
	}
	
	Transform NextFreePos(){
		foreach(Transform childPos in transform){
			if(childPos.childCount == 0){
				return childPos;
			}
		}
		return null;
	}
	
	bool AllMembersDead(){
		foreach(Transform childPos in transform){
			if(childPos.childCount > 0){
			return false;
			}
		}
		return true;
	}
}
