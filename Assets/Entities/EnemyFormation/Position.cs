﻿using UnityEngine;
using System.Collections;

public class Position : MonoBehaviour {
	public float Size = 1;

	void OnDrawGizmos(){
		Gizmos.DrawWireSphere(transform.position, Size);
	}
}
