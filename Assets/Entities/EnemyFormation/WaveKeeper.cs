﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaveKeeper : MonoBehaviour {

	public static int wave = 0;
	Text waveTxt;
	
	void Start(){
		waveTxt = GetComponent<Text>();
		Reset();
	}
	
	public void NextWave(){
		wave += 1;
		waveTxt.text = wave.ToString();
	}
	
	public static void Reset(){
		wave = 0;
	}
}
