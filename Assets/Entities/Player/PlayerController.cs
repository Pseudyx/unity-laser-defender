﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public float speed = 15.0f;
	public float padding = 1f;
	public GameObject projectile;
	public float projectileSpeed;
	public float fireRate = 0.02f;
	public float HP = 300f;
	
	public AudioClip FireSFX;
	LifeManager lifeManager;
	
	float minX = -5;
	float maxX = 5;
	
	void Start(){
		lifeManager = GameObject.Find("LifeManager").GetComponent<LifeManager>();
		float distance = transform.position.z - Camera.main.transform.position.z;
		minX = Camera.main.ViewportToWorldPoint(new Vector3(0,0,distance)).x + padding;
		maxX = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distance)).x - padding;
	}
	
	void LaserFire(){
		Vector3 offset = new Vector3(0,1,0);
		GameObject laser = Instantiate(projectile, transform.position+offset, Quaternion.identity) as GameObject;
		laser.rigidbody2D.velocity = new Vector3(0,projectileSpeed,0);
		AudioSource.PlayClipAtPoint(FireSFX, transform.position);
	}
	
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)){
			//InvokeRepeating("LaserFire", 0.00001f, fireRate);
			LaserFire();
		}
		if(Input.GetKeyUp(KeyCode.Space)){
			//CancelInvoke("LaserFire");
		}
	
		if(Input.GetKey(KeyCode.LeftArrow)){
			//transform.position += new Vector3(-speed * Time.deltaTime,0f,0f);
			transform.position += Vector3.left * speed * Time.deltaTime;
			
			
		} else if(Input.GetKey(KeyCode.RightArrow)){
			//transform.position += new Vector3(speed * Time.deltaTime,0f,0f);
			transform.position += Vector3.right * speed * Time.deltaTime;
		}
		
		float newX = Mathf.Clamp(transform.position.x,minX,maxX);
		transform.position = new Vector3(newX, transform.position.y, transform.position.z);
	}
	
	void OnTriggerEnter2D(Collider2D collision){
		Projectile laser = collision.gameObject.GetComponent<Projectile>();
		if(laser){
			HP -= laser.Damage;
			laser.Hit();
			lifeManager.ReduceLife();
			if(HP <= 0){
				LevelManager lvl = GameObject.Find("LevelManager").GetComponent<LevelManager>();
				lvl.LoadLevel("Win");
				Destroy(gameObject);
			}
			
			
			
		} 
	}
}
