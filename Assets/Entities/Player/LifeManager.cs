﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LifeManager : MonoBehaviour {
	public GameObject lifePrefab;
	
	// Use this for initialization
	void Start () {
		Reset();
	}
	
	void Reset(){
		foreach(Transform child in transform){
			GameObject life = Instantiate(lifePrefab, child.transform.position, Quaternion.identity) as GameObject;
			//life.transform.parent = child;
		}
	}
	
	public void ReduceLife(){
		int lives = transform.childCount;
		if(lives > 0){
			GameObject life = transform.GetChild(lives-1).gameObject;
			Destroy(life);
		}
	}
}
