﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {
	public GameObject projectile;
	public float projectileSpeed = 10;
	public float HP = 100f;
	public float Freq = 0.5f;	
	public int Points = 100;
	
	public AudioClip FireSFX;
	public AudioClip DestroySFX;
	
	ScoreKeeper scoreKeeper;
	
	void Start(){
		scoreKeeper = GameObject.Find("Score").GetComponent<ScoreKeeper>();
		
		HP *= WaveKeeper.wave;
		Points *= WaveKeeper.wave;
	}
	
	void Fire(){
		Vector3 startPosition = transform.position + new Vector3(0,-0.9f,0);
		GameObject laser = Instantiate(projectile, startPosition, Quaternion.identity) as GameObject;
		laser.rigidbody2D.velocity = new Vector2(0,-projectileSpeed);
		AudioSource.PlayClipAtPoint(FireSFX, transform.position);
	}
	
	void Update(){
		float prob =Time.deltaTime * Freq;
		if(Random.value < prob){
			Fire ();
		}
	}
	
	void OnTriggerEnter2D(Collider2D collision){
		Projectile laser = collision.gameObject.GetComponent<Projectile>();
		if(laser){
			HP -= laser.Damage;
			laser.Hit();
			if(HP <= 0){
				AudioSource.PlayClipAtPoint(DestroySFX, transform.position);
				Destroy(gameObject);
				scoreKeeper.Score(Points);
				
			}
			
			
			
		} 
	}
}
